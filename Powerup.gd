extends Node2D

signal powerup

enum Powerup {EXTEND, SHRINK, MULTIBALL, SPEED, STICK, BOMBBALL}

export var textures = [] 
export var speed : int
var rand_texture
var power_type

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	var paddle = get_tree().get_nodes_in_group("Paddle")
	paddle = paddle[0]
	connect("powerup", paddle, "get_power_up")
	var i = randi() % textures.size()
	rand_texture = textures[i]
	power_type = i
	$Sprite.texture = rand_texture
	var color
	match i:
		0:
			color = Color.blue
		1:
			color = Color.black
		2:
			color = Color.mediumvioletred
		3:
			color = Color.yellow
		4:
			color = Color.mediumspringgreen
		5:
			color = Color.red
	var gradient = GradientTexture.new()
	var mat =  $Particles2D.process_material.duplicate()
	gradient.set_gradient(Gradient.new())
	gradient.gradient.set_color(0, color)
	gradient.gradient.set_color(1, Color.white)
	mat.set_color_ramp(gradient)
	$Particles2D.set_process_material(mat)
	$Particles2D2.set_process_material(mat)
	
func _process(delta):
	position += Vector2(0, speed) * delta

func _on_Area2D_body_entered(body):
	if body.name == "Paddle":
		emit_signal("powerup", power_type)
		queue_free()

func _on_VisibilityEnabler2D_viewport_exited(viewport):
	yield(get_tree().create_timer(2), "timeout")
	queue_free()