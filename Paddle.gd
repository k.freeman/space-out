extends KinematicBody2D

signal extend
signal shrink
signal multiBall
signal bombBall
signal speedBall
signal sticky
signal firstBall
signal launch
signal move_stuck

export var max_speed = 600
export var start_speed = 400
export var acceleration = 50
export var sprites = []
export var sizes = []
export (Material) var sticky_mat
var speed = start_speed
var screen_size
var ballTimerUp = true
var extents 
var current_size = 1
var freeze = true
var firstBall = true
var is_sticky = false
var launch_dir = 1

func _ready():
	screen_size = get_viewport_rect().size
	extents = $CollisionShape2D.shape.get_extents()
	add_to_group("Paddle")
	
func _process(delta):
	var velocity = Vector2()
	if !freeze:
		# If right or left are pressed, add 1 to the velocity vector in the respective direction
		if Input.is_action_pressed("ui_right"):
			velocity.x += 1
			launch_dir = 1
		if Input.is_action_pressed("ui_left"):
			velocity.x -= 1
			launch_dir = -1
		# Normalize the Vector
		if velocity.length() > 0:
			velocity = velocity.normalized() * speed 
		# If either left or right have been pressed, increment speed by accelerate and clamp 
		# between start and max speed 
		if Input.is_action_pressed("ui_right") || Input.is_action_pressed("ui_left"):
			speed += acceleration
			speed = clamp (speed, start_speed, max_speed)
		# If left or right were just pressed, reset the speed
		if Input.is_action_just_pressed("ui_right") || Input.is_action_just_pressed("ui_left"):
			speed = start_speed
		# Player presses space and the ball is not is the air and the timer has expired
		if Input.is_action_just_pressed("ui_accept"):
			launch()
			if firstBall:
				emit_signal("firstBall")
				firstBall = false
	# Add the new velocity to position, giving up movement
	position += velocity * delta
	position.x = clamp(position.x, 0 + $Sprite.texture.get_width()/2, screen_size.x - $Sprite.texture.get_width()/2)
	emit_signal("move_stuck", velocity * delta)
	
# reset position when level starts
func on_Start_level(pos):
	position = pos
	firstBall = true
	ballTimerUp = true
		
func new_ball():
	$BallTimer.start()
	is_sticky = false
	$Sprite.material = null
	current_size = 1
	update_current_size()
	
func _on_BallTimer_timeout():
	ballTimerUp = true
	var ballScene = preload("res://Ball.tscn")
	var ballNode = ballScene.instance()
	get_parent().add_child(ballNode)
	ballNode.is_stuck = true
	ballNode.position = Vector2(position.x, position.y - $Sprite.texture.get_height())
	ballTimerUp = false
		
func launch():
	emit_signal("launch", launch_dir)
	
func get_power_up(type):
	match type:
		0:
			extend()
			emit_signal("extend", 0)
		1:
			shrink()
			emit_signal("shrink", 1)
		2:
			multi_ball()
			emit_signal("multiBall", 2)
		3:
			speed_ball()
			emit_signal("speedBall", 3)
		4:
			sticky()
			emit_signal("sticky", 4)
		5:
			bomb_ball()
			emit_signal("bombBall", 5)
			
func update_current_size():
	$Sprite.texture = sprites[current_size]
	$CollisionShape2D.shape.extents = sizes[current_size]
	extents = $CollisionShape2D.shape.get_extents()

func extend():
	$Timers/ExtendShrinkTimer.start(15)
	if current_size < 2:
		current_size += 1
	update_current_size()
	yield($Timers/ExtendShrinkTimer,"timeout")
	if current_size == 2:
		current_size -= 1 
		update_current_size()
	
func sticky():
	is_sticky = true
	$Sprite.material = sticky_mat
	$Timers/StickyTimer.start(15)
	yield($Timers/StickyTimer,"timeout")
	$Sprite.material = null
	is_sticky = false

func shrink():
	$Timers/ExtendShrinkTimer.start(15)
	if current_size > 0:
		current_size -= 1
	update_current_size()
	yield($Timers/ExtendShrinkTimer,"timeout")
	if current_size == 0:
		current_size += 1
		update_current_size()
	
func bomb_ball():
	$Timers/BombBallTimer.start(15)
	get_tree().call_group("Balls", "start_bomb_ball")
	yield($Timers/BombBallTimer, "timeout")
	get_tree().call_group("Balls", "stop_bomb_ball")
	
func speed_ball():
	get_tree().call_group("Balls", "set_speed_ball")

func multi_ball():
	get_tree().call_group("Balls", "multi_ball")
	
