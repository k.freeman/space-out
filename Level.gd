extends Node

signal restart
signal levelComplete

var Paddle
var paddleInstance
var brick_count : int
var game_over = false
var level_complete = false
var sudden_death = false
var timer = 0

var BrickPositions = []
#warning-ignore:unused_class_variable
var level1 = [[0, 2, 0, -1, 0, 2, 0, -1, 0, 2, 0, -1, 0, 2, 0,],
			  [0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0,],
			  [0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0,],
			  [0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0,],
			  [0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0, -1, 0, 0, 0,],
			  [0, 2, 0, -1, 0, 2, 0, -1, 0, 2, 0, -1, 0, 0, 0,]]
			
#warning-ignore:unused_class_variable
var level2 = [[-1, 4, -1, 0, 2, 0, -1, -1, -1, 0, 2, 0, -1, 4, -1,],
			  [4, -1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, 4,],
			  [-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1,],
			  [2, 0, 0, 0, -1, 0, 0, 0, 0, 0, -1, 0, 0, 0, 2,],
			  [0, 0, 0, -1, 2, -1, 0, 0, 0, -1, 2, -1, 0, 0, 0,],
			  [0, 0, -1, 4, 4, 4, -1, 0, -1, 4, 4, 4, -1, 0, 0,]]
			
#var level3 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
#			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
#			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
#			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
#			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
#			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,]]

var level3 = [[0, 0, 2, 0, 0, 0, -1, 1, -1, 0, 0, 0, 0, 2, 0,],
			  [0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0,],
			  [0, 2, 0, 0, 0, 1, 2, 1, 2, 1, 0, 0, 0, 2, 0,],
			  [4, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 4,],
			  [-1, 4, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 4, -1,],
			  [-1, -1, 4, 2, 0, 0, 0, 1, 0, 0, 0, 2, 4, -1, -1,]]

var level4 = [[4, 4, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 4, 4,],
			  [4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4,],
			  [-1, 0, 0, 0, 0, 2, -1, -1, -1, 2, 0, 0, 0, 0, -1,],
			  [-1, 0, 0, 0, 0, 0, -1, -1, -1, 0, 0, 0, 0, 0, -1,],
			  [4, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 4,],
			  [4, 4, 1, 1, 0, 0, 2, 0, 2, 0, 0, 1, 1, 4, 4,]]
			
var level5 = [[-1, -1, 1, 0, 4, 0, 0, 2, 0, 0, 4, 0, 1, -1, -1,],
			  [-1, 1, 0, 4, 0, 0, 0, 0, 0, 0, 0, 4, 0, 1, -1,],
			  [1, 0, 4, 2, 0, 0, 0, 0, 0, 0, 0, 2, 4, 0, 1,],
			  [0, 4, 0, 0, 0, 0, 2, 0, 2, 0, 0, 0, 0, 4, 0,],
			  [4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4,],
			  [0, 0, 0, 2, 1, 1, 1, 0, 1, 1, 1, 2, 0, 0, 0,]]
			
var level6 = [[0, 0, 0, 0, 0, 0,-1, -1,-1, 0, 0, 0, 0, 0, 0,],
			  [1, 0, 1, 0, 0, 2, 0, -1, 0, 2, 0, 0, 1, 0, 1,],
			  [1, 0, 1, 2, 0, -1, 1, 1, 1, -1, 0, 2, 1, 0, 1,],
			  [1, 0, 1, 2, 0, -1, 1, 1, 1, -1, 0, 2, 1, 0, 1,],
			  [1, 0, 4, 0, 4, 2, 0, -1, 0, 2, 4, 0, 4, 0, 1,],
			  [0, 0, 4, 4, 4, 0, -1, -1, -1, 0, 4, 4, 4, 0, 0,]]

var level7 = [[-1, 0, 4, 4, 4, 0, 0, 0, 0, 0, 4, 4, 4, 0, -1,],
			  [0, 0, 0, 2, 0, 2, 1, 1, 1, 2, 0, 2, 0, 0, 0,],
			  [4, 0, 0, 0, 0, 0, 1, -1, 1, 0, 0, 0, 0, 0, 4,],
			  [-1, 4, 0, 0, 0, 2, 1, 1, 1, 2, 0, 0, 0, 4, -1,],
			  [-1, -1, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, -1, -1,],
			  [1, -1, -1, 4, 0, 2, 0, 0, 0, 2, 0, 4, -1, -1, 1,]]	
			
var level8 = [[4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 2, 4, 4, 4,],
			  [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0,],
			  [1, -1, -1, -1, 1, 0, 0, 0, 0, 0, 1, -1, -1, -1, 1,],
			  [1, -1, -1, -1, 1, 0, 2, 0, 2, 0, 1, -1, -1, -1, 1,],
			  [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0,],
			  [4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,]]				

var level9 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,]]

var level10 = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,],
			  [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,]]
			
var levels = [level1, level2, level3, level4, level5, level6, level7, level8]

# Called when the node enters the scene tree for the first time.
func _ready():
#warning-ignore:unused_variable
	for x in range(15):
		var col = []
		col.resize(10)
		BrickPositions.append(col)
	Paddle = preload("res://Paddle.tscn")
	paddleInstance = Paddle.instance()
	add_child(paddleInstance)
	paddleInstance.name = "Paddle"
	paddleInstance.connect("extend", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("shrink", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("multiBall", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("bombBall", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("sticky", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("speedBall", get_parent().gui_instance, "powerup_display")
	paddleInstance.connect("firstBall", self, "start_level_timer")
#warning-ignore:return_value_discarded
	get_parent().connect("newBall", paddleInstance, "new_ball")
#warning-ignore:return_value_discarded
	connect("restart", get_parent(), "restart_level")

# Called every frame. 'delta' is the elapsed time since the previous frame.
#warning-ignore:unused_argument
func _process(delta):
	if brick_count <= 0 && !level_complete:
		level_complete = true
		end_level()
	if brick_count < 5 && !sudden_death && !level_complete:
		sudden_death()
	if game_over:
		if Input.is_action_just_pressed("ui_accept"):
			emit_signal("restart")
			queue_free()

# Call the build function to spawn all the bricks for the current level,
# reset the paddle position to the starting position
# reset the ball position to the paddle
# reset the LevelStartTimer
func init_level(level):
	if !(level > levels.size()):
		$Polygon2D/Container/AnimationPlayer.play("Level Start")
		$Polygon2D/Container/Label.text = "Level %s" % (level + 1)
		sudden_death = false
		level_complete = false
		game_over = false
		brick_count = 0
		var brick = preload("res://Brick.tscn")
		$LevelStartTimer.start()
		var pos = $PaddleStartPos.position
		paddleInstance.on_Start_level(pos)
		for x in range(6):
			for y in range(15):
				var brick_node = brick.instance()
				add_child(brick_node)
				brick_node.set_brick(levels[level][x][y])
				brick_node.position = Vector2(y * 34 + 80, x * 18 + 80)
				if brick_node.brick_type != -1 && brick_node.brick_type != 1:
					brick_count += 1
					brick_node.add_to_group("Bricks")
				elif brick_node.brick_type == 1:
					brick_node.add_to_group("Unbreakable")
		get_parent().emit_signal("newBall")
	
# All blocks have been destroyed, move on to the next level!
func end_level():
	$TimeElapsed.stop()
	emit_signal("levelComplete", timer)
	for ball in get_tree().get_nodes_in_group("Balls"):
		ball.queue_free()
	for brick in get_tree().get_nodes_in_group("Unbreakable"):
		brick.queue_free()
	paddleInstance.freeze = true
	
#warning-ignore:function_conflicts_variable
func sudden_death():
	sudden_death = true
	yield(get_tree().create_timer(15),"timeout")
	if !level_complete && !game_over:
		$Polygon2D/Container/AnimationPlayer.play("Level Start")
		$Polygon2D/Container/Label.text = "SUDDEN DEATH!"
		randomize()
		var bricks =  get_tree().get_nodes_in_group("Bricks")
		bricks[randi() % bricks.size()].init_sudden_death()
	
func _on_LevelStartTimer_timeout():
	paddleInstance.freeze = false
	pass # Replace with function body.
	
func start_level_timer():
	$TimeElapsed.start()
	$"Launch!".hide()
	
func _on_TimeElapsed_timeout():
	timer += 1