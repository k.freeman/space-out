extends Node

signal newBall

var score
var ball_count
var multiplier
var max_combo
var current_combo
var level
var gui
var gui_instance
var level_scene
var current_level
var current_level_instance
var camera
var shake_cam = false
var shake_mul = 0
var in_game_over = false
var levelComplete = false
var speed_ball_bonus = 0

func _ready():
	camera = $Camera2D
	camera.current = true
	score = 0
	current_level = 0
	ball_count = 4
	multiplier = 1
	max_combo = 0
	current_combo = 0
	gui = preload("res://GUI.tscn")
	gui_instance = gui.instance()
	level_scene = preload("res://Levels/Level1.tscn")
	current_level_instance = level_scene.instance()
	add_child(current_level_instance)
	current_level_instance.connect("levelComplete", self, "level_complete")
	add_child(gui_instance)
	gui_instance.connect("mainmenu", self, "return_to_main_menu")
	init_gui()
	current_level_instance.init_level(current_level)
	
#warning-ignore:unused_argument
func _process(delta):
	if shake_cam:
		camera.set_offset(Vector2( \
			rand_range(-1,1) * shake_mul, \
			rand_range(-1,1) * shake_mul \
		))
	if in_game_over:
		if Input.is_action_just_pressed("ui_accept"):
			in_game_over = false
			restart_level()
		if Input.is_action_just_pressed("ui_cancel"):
#warning-ignore:return_value_discarded
			get_tree().change_scene("res://MainMenu.tscn")
			queue_free()
	if Input.is_action_just_pressed("pause") && !levelComplete && !in_game_over:
		gui_instance.pause_menu_display()
		get_tree().paused = true
	
func init_gui():
	score = 0
	ball_count = 4
	multiplier = 1 
	level = 1
	gui_instance.update_score(score)
	gui_instance.update_balls(ball_count)
	gui_instance.update_multiplier(multiplier)
	gui_instance.update_level(level)
	
func add_score(val):
	score += (val * multiplier) + speed_ball_bonus
	current_combo += 1
	if current_combo > max_combo:
		max_combo = current_combo
	gui_instance.update_score(score)
	
func inc_multiplier():
	multiplier += 1
	if multiplier > 8:
		multiplier = 8
	gui_instance.update_multiplier(multiplier)
	
func reset_multiplier():
	multiplier = 1
	current_combo = 0
	gui_instance.update_multiplier(multiplier)
	
func instance_scorelabel(text, position):
	gui_instance.instance_scorelabel(text, position)

func lost_ball():
	reset_multiplier()
	if get_tree().get_nodes_in_group("Balls").size() == 0 && !levelComplete:
		ball_count -= 1
		if ball_count < 0:
			ball_count = 0
			game_over()
		else:
			emit_signal("newBall")
		gui_instance.update_balls(ball_count)
	
func camera_shake(val, time):
	$ShakeTimer.wait_time = time
	$ShakeTimer.start()
	shake_cam = true
	shake_mul = val
	
func _on_ShakeTimer_timeout():
	shake_cam = false
	camera.set_offset(Vector2(0,0))
	
func restart_level():
	add_score(-1*score)
	ball_count = 4
	gui_instance.update_balls(ball_count)
	remove_child(current_level_instance)
	remove_child(gui_instance)
	gui_instance.queue_free()
	current_level_instance.queue_free()
	gui_instance = gui.instance()
	current_level_instance = level_scene.instance()
	add_child(current_level_instance)
	current_level_instance.connect("levelComplete", self, "level_complete")
	add_child(gui_instance)
	gui_instance.connect("mainmenu", self, "return_to_main_menu")
	init_gui()
	current_level_instance.init_level(current_level)
	
func level_complete(time):
	levelComplete = true
	var level_complete_screen = preload("res://LevelCompleteScreen.tscn").instance()
	level_complete_screen.time = time
	level_complete_screen.max_combo = max_combo
	level_complete_screen.score = score
	level_complete_screen.extra_balls = 0
	add_child(level_complete_screen)
	level_complete_screen.connect("transition", self, "load_next_level")
	
func load_next_level():
	current_level += 1
	if current_level > current_level_instance.levels.size():
		return_to_main_menu()
	current_level_instance.init_level(current_level)
	gui_instance.update_level(current_level+1)
	levelComplete = false
	
func game_over():
	gui_instance.start_game_over()
	current_level_instance.game_over = true
	in_game_over = true
	
func return_to_main_menu():
	var menu = load("res://MainMenu.tscn").instance()
	get_parent().add_child(menu)
	queue_free()