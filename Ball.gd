extends KinematicBody2D

signal lostBall
signal hitPaddle

#warning-ignore:unused_class_variable
var speed = 250
var sprite_width
var screen_size
var velocity
var paddle_sprite
var is_multi = false
var is_stuck = false
var bomb_state = false
var paddle
export var sounds = []

func _ready():
	velocity = Vector2(1,-1)
	sprite_width = $Sprite.texture.get_width()
	screen_size = get_viewport_rect().size
	paddle = get_node("../Paddle")
	paddle_sprite = get_node("../Paddle/Sprite")
	get_node("../Paddle").connect("launch", self, "launch")
	get_node("../Paddle").connect("move_stuck", self, "move_if_stuck")
#warning-ignore:return_value_discarded
	connect("lostBall", get_parent().get_parent(), "lost_ball")
#warning-ignore:return_value_discarded
	connect("hitPaddle", get_parent().get_parent(), "reset_multiplier")
	add_to_group("Balls")
	
func _process(delta):
	if is_stuck:
		$IndicatorSprite.show()
		if paddle.launch_dir == 1:
			$IndicatorSprite/AnimationPlayer.play("IndicatorArrow_right")
		if paddle.launch_dir == -1:
			$IndicatorSprite/AnimationPlayer.play("IdicatorArrow_left")
	else:
		$IndicatorSprite.hide()
		$IndicatorSprite/AnimationPlayer.stop()
	if bomb_state && !is_stuck:
		if Input.is_action_just_pressed("ui_accept"):
			bomb_ball_explode()
	
	
func _physics_process(delta):
	velocity = velocity.normalized() * speed
	# if not currently stuck to the paddle
	if !is_stuck:
		var collision = move_and_collide(velocity * delta)
		if collision:
			# if we hit the paddle 
			if collision.collider.name == "Paddle":
				hit_paddle()
				# check if it's sticky, if it is set new stuck position
				if collision.collider.is_sticky:
					position = Vector2(collision.position.x, position.y)
					is_stuck = true
					set_collision_mask_bit(2, false)
				# else calculate new velocity vector
				else:
					var new_vel_vec = Vector2()
					var reflect_dir = 1
					# if the ball hit the top of the paddle and not directly on a corner
					if collision.position.y <= collision.collider.position.y - collision.collider.extents.y \
					&& !(collision.position.x < collision.collider.position.x - collision.collider.extents.x || 
						  collision.position.x > collision.collider.position.x + collision.collider.extents.x):
						# if it hit on the right side and is moving right 
						# or it hit on the left side and is moving left
						if collision.position.x >= collision.collider.position.x && velocity.x >= 0 \
						|| collision.position.x < collision.collider.position.x && velocity.x < 0:
							reflect_dir = 1
						# if it hit on the left side and is moving right
						# or it hit on the right side and is moving left
						if collision.position.x < collision.collider.position.x && velocity.x >= 0 \
						|| collision.position.x >= collision.collider.position.x && velocity.x < 0:
							reflect_dir = -1
						# if it hit on the left end side or the right end side, find a vec to the ball and reflect appropriately
						# for our new vec, gives variation to the angle of the ball's vector
						if collision.position.x < collision.collider.position.x - paddle_sprite.texture.get_width()/4 \
						|| collision.position.x > collision.collider.position.x + paddle_sprite.texture.get_width()/4:
							new_vel_vec = (position - collision.collider.position).normalized() * Vector2(reflect_dir, 1)
						# else if it hits within the "center area" reflect vector over the x-axis
						else:
							new_vel_vec = velocity * Vector2(1,-1)
						position.y -= 5
					else:
						print("hit the side/corner?")
						new_vel_vec = velocity * Vector2(-1,1)
						if position.x < collision.collider.position.x:
							position.x -= 2
						if position.x > collision.collider.position.x:
							position.x += 2
					velocity = new_vel_vec
					emit_signal("hitPaddle")
			# we hit a brick, so just reflect based on the normal of the collision.
			else: 
				if collision.collider.name.find("Brick") != -1:
					collision.collider.was_hit(false)
				if collision.normal.x != 0:
					velocity.x *= -1
				if collision.normal.y != 0:
					velocity.y *= -1
			check_angle()
		
#warning-ignore:unused_argument
func _on_VisibilityNotifier2D_viewport_exited(viewport):
	remove_from_group("Balls")
	if get_tree().get_nodes_in_group("Balls").size() == 0:
		emit_signal("lostBall")
	$DestroyTimer.start()
	$DestroyExplosion.emitting = true
	$Sounds.stream = sounds[0]
	$Sounds.pitch_scale += rand_range(-.2,.2)
	$Sounds.play(0)
	get_parent().get_parent().camera_shake(6, .5)
	
func _on_DestroyTimer_timeout():
	queue_free()
	
func multi_ball():
	var new_ball = load("res://Ball.tscn").instance()
	get_parent().add_child(new_ball)
	randomize()
	new_ball.position = position
	if velocity.x >= 0:
		new_ball.velocity.x = -1
	else:
		new_ball.velocity.x = 1
	new_ball.velocity.y = -1
	new_ball.is_multi = true
	
func remove_multi():
	if is_multi:
		queue_free()

func set_speed_ball():
	speed += 125
	get_parent().get_parent().speed_ball_bonus += 100
	yield(get_tree().create_timer(10),"timeout")
	speed -= 125
	get_parent().get_parent().speed_ball_bonus -= 100
	
func hit_paddle():
	if !is_stuck:
		$HitPaddleTimer.start()
		self.set_collision_mask_bit(2, false)
	
func _on_HitPaddleTimer_timeout():
	set_collision_mask_bit(2, true)
	
func check_angle():
	var angle = rad2deg(velocity.angle())
	if angle >=0:
		if angle > 160:
			angle = 160
		if angle < 20:
			angle = 20
	if angle < 0:
		if angle < -160:
			angle = -160
		if angle > -20:
			angle = -20
	angle = deg2rad(angle)
	velocity = (Vector2(1,0).rotated(angle)).normalized()
	
func launch(launch_dir):
	if is_stuck:
		position += Vector2(0,-5)
		velocity = Vector2(launch_dir, -1)
		is_stuck = false
		$HitPaddleTimer.start()
		
func move_if_stuck(velocity):
	if is_stuck:
		position += velocity
		position.x = clamp(position.x, 0 + $Sprite.texture.get_width()/2, screen_size.x - $Sprite.texture.get_width()/2)
		
func start_bomb_ball():
	$AnimationPlayer.play("Bomb_ball")
	bomb_state = true
	
func stop_bomb_ball():
	bomb_state = false
	$AnimationPlayer.stop()
	$Sprite.hframes = 1
	$Sprite.frame = 0
	$Sprite.texture = load("res://Assets/Sprites/ball.png")
	var gradient = GradientTexture.new()
	var mat =  $TrailParticles.process_material.duplicate()
	gradient.set_gradient(Gradient.new())
	gradient.gradient.set_color(0, Color.yellow)
	gradient.gradient.set_color(1, Color.white)
	mat.set_color_ramp(gradient)
	$TrailParticles.set_process_material(mat)

func bomb_ball_explode():
	stop_bomb_ball()
	$Area2D/AnimationPlayer.play("explosion")
	$Sounds.stream = sounds[1]
	$Sounds.pitch_scale += rand_range(-.2,.2)
	$Sounds.play(0)
	get_parent().get_parent().camera_shake(5,.8)
	for body in $Area2D.get_overlapping_bodies():
		if body.name.find("Brick") != -1:
			body.was_hit(true)


func _on_Sounds_finished():
	$Sounds.pitch_scale = 1
	pass # Replace with function body.
