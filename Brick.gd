extends KinematicBody2D
class_name Brick

enum BrickType {NORMAL, UNBREAKABLE , POWERUP , BONUS, MULTIHIT, DISABLED = -1}
var brick_type
var in_sudden_death = false
var multihit = 3
var dead = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.
	
func enter():
	yield(get_tree().create_timer(rand_range(0.1,0.5)),"timeout")
	match brick_type:
		BrickType.NORMAL:
			$AnimationPlayer.play("WhiteBlock_enter")
		BrickType.POWERUP:
			$AnimationPlayer.play("PowerUpBlock_enter")
			yield($AnimationPlayer,"animation_finished")
			$AnimationPlayer.play("PowerUpBlockPulse")
		BrickType.UNBREAKABLE:
			$Sprite.texture = preload("res://Assets/Sprites/Brick_warning.png")
			$Sprite.hframes = 1
			$Sprite.frame = 0
		BrickType.DISABLED:
			queue_free()
		BrickType.MULTIHIT:
			$Sprite.texture = preload("res://Assets/Sprites/Brick_multi_hit.png")
			$Sprite.hframes = 6
			$Sprite.frame = 0
	
func set_brick(val):
	brick_type = val
	enter()
			
func was_hit(bomb):
	match brick_type:
		BrickType.NORMAL:
			var gui = get_parent().get_parent()
			gui.instance_scorelabel(100, position)
			gui.add_score(100) 
			gui.inc_multiplier()
			explosion(true, bomb)
		BrickType.UNBREAKABLE:
			if bomb:
				var gui = get_parent().get_parent()
				gui.instance_scorelabel(300, position)
				gui.add_score(300)
				gui.inc_multiplier()
				explosion(false, bomb)
		BrickType.POWERUP:
			var gui = get_parent().get_parent()
			gui.instance_scorelabel(200, position)
			gui.add_score(200)
			gui.inc_multiplier()
			explosion(true, bomb)
			drop_powerup()
		BrickType.MULTIHIT:
			if bomb:
				var gui = get_parent().get_parent()
				gui.instance_scorelabel(300, position)
				gui.add_score(300)
				gui.inc_multiplier()
				explosion(true, bomb)
			else:
				$Explosion.pitch_scale += rand_range(1,1.5) 
				match multihit:
					3:
						$AnimationPlayer.play("Multihit_hit1")
						$Explosion.play(0)
						multihit -= 1
					2: 
						$AnimationPlayer.play("Multi_hit2")
						$Explosion.play(0)
						multihit -= 1
					1: 
						$Explosion.play(0)
						$AnimationPlayer.play("Multi_hit3")
						yield($AnimationPlayer, "animation_finished")
						var gui = get_parent().get_parent()
						gui.instance_scorelabel(300, position)
						gui.add_score(300)
						gui.inc_multiplier()
						explosion(true, bomb)
			
func drop_powerup():
	var powerup = load("res://Powerup.tscn")
	var inst = powerup.instance()
	get_parent().add_child(inst)
	inst.position = position

func _on_DestroyTimer_timeout():
	queue_free()
	
func explosion(call_another, bomb):
	if dead:
		return
	else:
		dead = true
		if brick_type != BrickType.UNBREAKABLE:
			remove_from_group("Bricks")
			get_parent().brick_count -= 1
		if in_sudden_death && call_another:
			var bricks = get_tree().get_nodes_in_group("Bricks")
			if bricks.size() != 0:
				bricks[randi() % bricks.size()].init_sudden_death()
		remove_child($Sprite)
		$CollisionShape2D.disabled = true
		$Particles2D.emitting = true
		$Particles2D.restart()
		$DestroyTimer.start()
		get_parent().get_parent().camera_shake(3, 0.15)
		if !bomb:
			$Explosion.pitch_scale = 1 + rand_range(-.2,.6)
			$Explosion.playing = true
			
func init_sudden_death():
	if get_tree().get_nodes_in_group("Bricks").size() < 5:
		$ColorRect/AnimationPlayer.play("AlphaFlash")
		$ColorRect/inc_anim.start(1.2)
		$ColorRect/SuddenDeathTimer.start(6)
		in_sudden_death = true

func _on_inc_anim_timeout():
	$ColorRect/AnimationPlayer.playback_speed += .1
	$ColorRect/inc_anim.start(1.2)

func _on_SuddenDeathTimer_timeout():
	remove_child($ColorRect)
	remove_from_group("Bricks")
	explosion(true, false)
#	get_tree().call_group("Bricks", "explosion", false, true)
	
	