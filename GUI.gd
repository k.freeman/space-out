extends Node

signal mainmenu

var level_number
var ball_count
var score
var multiplier
var game_over
var level_complete
var scorelabel
var paused
var pause_menu_selection = 0
export var sounds = []

func _ready():
	level_number = $"MarginContainer/HBoxContainer/VBoxContainer/Level Number"
	ball_count = $MarginContainer/HBoxContainer/VBoxContainer/Balls
	score = $MarginContainer/HBoxContainer2/VBoxContainer/Score
	multiplier = $MarginContainer/HBoxContainer2/VBoxContainer/Multiplier
	game_over = $"CenterContainer/Game Over Text"
	game_over.visible_characters = 0
	scorelabel = load("res://ScoreLabel.tscn")
	
func _process(delta):
	if Input.is_action_just_pressed("ui_page_down"):
		start_game_over()
	if paused:
		$PauseScreen.show()
		if Input.is_action_just_pressed("ui_down"):
			pause_menu_selection += 1
			if pause_menu_selection > 2:
				pause_menu_selection = 2
			update_selection()
		if Input.is_action_just_pressed("ui_up"):
			pause_menu_selection -= 1
			if pause_menu_selection < 0:
				pause_menu_selection = 0
			update_selection()
		if Input.is_action_just_pressed("ui_accept"):
			confirm_selection()
		if Input.is_action_just_pressed("ui_cancel"):
			$PauseScreen/AudioStreamPlayer.stream = sounds[1]
			$PauseScreen/AudioStreamPlayer.play(0)
			emit_signal("unpause")
			paused = false
			get_tree().paused = false
	else:
		$PauseScreen.hide()
	
func update_score(val):
	score.text = "Score: %s" % str(val)

func update_balls(val):
	ball_count.text = "Balls: %s" % str(val) 

func update_level(val):
	level_number.text = "Level: %s" % str(val)

func update_multiplier(val):
	multiplier.text = "x%s" % str(val)
	
func instance_scorelabel(text, position):
	var instance = scorelabel.instance()
	add_child(instance)
	instance.change_text(text*get_parent().multiplier)
	match get_parent().multiplier:
		1:
			continue
		2:
			continue
		3:	
			pass
		4:
			instance.set_anim("TextColorFlash2")
		5:
			instance.set_anim("TextColorFlash2")
		6:
			instance.set_anim("TextColorFlash2")
		7:
			instance.set_anim("TextColorFlash2")
		8:
			instance.set_anim("TextColorFlash3")
	instance.position = position
	
func start_game_over():
	game_over.visible_characters = 0
	$CenterContainer/TextTimer.start()
	$PauseScreen/AudioStreamPlayer.stream = sounds[2]
	$PauseScreen/AudioStreamPlayer.play()
	$AnimationPlayer.play("MiddleColorRectOpacity")
	yield(get_tree().create_timer(.5), "timeout")
	$Particles2D.emitting = true

func _on_TextTimer_timeout():
	if game_over.visible_characters != 10:
		game_over.visible_characters += 1
		$CenterContainer/TextTimer.start()
		
func powerup_display(type):
	var anim
	match type:
		0:
			anim = "Extend"
		1:
			anim = "Shrink"
		2:
			anim = "Multi Ball"
		3:
			anim = "SpeedBall"
		4:
			anim = "Sticky Ball"
		5:
			anim = "Bomb Ball"
	if $PowerUPBar/AnimationPlayer.is_playing():
		$PowerUPBar/AnimationPlayer.stop()
	$PowerUPBar/AnimationPlayer.play("Enterfromleft")
	if $PowerUPBar/MarginContainer/Label/AnimationPlayer.is_playing():
		$PowerUPBar/MarginContainer/Label/AnimationPlayer.stop()
	$PowerUPBar/MarginContainer/Label/AnimationPlayer.play(anim)
	
func pause_menu_display():
	$PauseScreen/AudioStreamPlayer.stream = sounds[0]
	$PauseScreen/AudioStreamPlayer.play(0)
	paused = true

func update_selection():
	var anim = $PauseScreen/ColorRect/VBoxContainer/AnimationPlayer
	$PauseScreen/AudioStreamPlayer.stream = sounds[3]
	$PauseScreen/AudioStreamPlayer.play(0)
	match pause_menu_selection:
		0:
			anim.play("continue")
		1:
			anim.play("options")
		2:
			anim.play("mainmenu")
			
func confirm_selection():
	$PauseScreen/AudioStreamPlayer.stream = sounds[4]
	match pause_menu_selection:
		0:
			$PauseScreen/AudioStreamPlayer.stream = sounds[1]
			$PauseScreen/AudioStreamPlayer.play(0)
			paused = false
			get_tree().paused = false
		1:
			$PauseScreen/AudioStreamPlayer.play(0)
			pass
		2:
			$PauseScreen/AudioStreamPlayer.play(0)
			yield(get_tree().create_timer(.2),"timeout")
			get_tree().paused = false
			emit_signal("mainmenu")