extends ColorRect

signal transition

var time = 0
var max_combo = 0
var extra_balls = 0
var score = 0
var val1 = 0
var val2 = 0
var val3 = 0
var val4 = 0
var update_score = false
var end = false
var cur_node 
export var left_labels = []
export var right_labels = []
export var sounds = []
# Called when the node enters the scene tree for the first time.
func _ready():
	$AudioStreamPlayer.play(0)
	$AudioStreamPlayer2.play(0)
	$AnimationPlayer.queue("ToWhite")
	$AnimationPlayer.queue("LevelCompleteBannerEnter")
	$AnimationPlayer.queue("GrowCenterRects")
	$AnimationPlayer.queue("Time to complete")

#warning-ignore:unused_argument
func _process(delta):
	if update_score:
		if val4 <= score:
			val4 += 300
			cur_node.text = str(val4)
		else:
			if !end:
				print("inc_score")
				$Inc_sounds.stream = sounds[1]
				$Inc_sounds.play(0)				
			cur_node.text = str(score)
			end = true
			update_score = false
	if end: 
		$Polygon2D2/Label2.visible = true
	if Input.is_action_just_pressed("ui_accept") && !end:
		$Inc_sounds.playing = false
		skip()
	elif Input.is_action_just_pressed("ui_accept") && end:
		$Inc_sounds.playing = false
		$AudioStreamPlayer.stream = load("res://Assets/sfx/menu_beep_3.wav")
		$AudioStreamPlayer.play()
		$AnimationPlayer.playback_speed = 2
		$AnimationPlayer.play_backwards("LevelCompleteBannerEnter")
		yield($AnimationPlayer, "animation_finished")
		$AnimationPlayer.play_backwards("GrowCenterRects")
		for node in left_labels:
			get_node(node).visible = false
		for node in right_labels:
			get_node(node).visible = false
		yield($AnimationPlayer, "animation_finished")
		$AnimationPlayer.play("FadeToBlack")

func init_vals(time, combo, balls, score):
	time = time
	max_combo = combo
	extra_balls = balls
	score = score

func inc_visible_chars(node):
	var n = get_node(node)
	n.visible_characters += 1
	if n.visible_characters == n.text.length():
		next_anim(node)

func inc_timer(node):
	var n = get_node(node)
	if val1 <= time:
		val1 += 3
		var s
		if val1 % 60 < 10:
			s = "0"
		else: 
			s = ""
		n.text = "%s:%s%s" % [str(val1 / 60), val1 % 60, s]
	else:
		var s
		if time % 60 < 10:
			s = "0"
		else: 
			s = ""
		n.text = "%s:%s%s" % [str(time / 60), time % 60, s]
		$Inc_sounds.stream = sounds[1]
		$Inc_sounds.play(0)
		next_anim(node)
	
func inc_combo(node):
	var n = get_node(node)
	if val2 <= max_combo:
		val2 += 2
		n.text = str(val2)
	else:
		$Inc_sounds.stream = sounds[1]
		$Inc_sounds.play(0)
		n.text = str(max_combo)
		next_anim(node)
		
func inc_ball(node):
	var n = get_node(node)
	if val3 != extra_balls:
		val3 += 1
		n.text = str(val3)
	else:
		print("inc_ball")
		$Inc_sounds.stream = sounds[1]
		$Inc_sounds.play(0)
		next_anim(node)
		
func next_anim(node):
	var n = get_node(node)
	match n.name:
		"Time":
			$AnimationPlayer.play("inc_timer")
			$Inc_sounds.stream = sounds[0]
			$Inc_sounds.play(0)
		"Max Combo":
			$AnimationPlayer.play("inc_combo")
			$Inc_sounds.stream = sounds[0]
			$Inc_sounds.play(0)
		"ExtraBalls":
			$AnimationPlayer.play("inc_balls")
			$Inc_sounds.stream = sounds[0]
			$Inc_sounds.play(0)
		"Score":
			$AnimationPlayer.play("inc_score")
			$Inc_sounds.stream = sounds[0]
			$Inc_sounds.play(0)
			cur_node = get_node("ReferenceRect/MarginContainer/VBoxContainer2/ScoreVal")
			update_score = true
		"TimeVal":
			$AnimationPlayer.play("MaxCombo")
		"MaxComboVal":
			$AnimationPlayer.play("Extra Balls")
		"BallVal":
			$AnimationPlayer.play("Score")
		"ScoreVal":
			end = true
			
			
func skip():
	var queue = PoolStringArray([$AnimationPlayer.current_animation]) + $AnimationPlayer.get_queue()
	for anim in queue:
		$AnimationPlayer.play(anim)
		$AnimationPlayer.advance($AnimationPlayer.current_animation_length - $AnimationPlayer.current_animation_position)
	for label in left_labels:
		get_node(label).visible_characters = get_node(label).text.length()
	var s
	if time % 60 < 10:
			s = "0"
	else: 
		s = ""
	update_score = false
	get_node(right_labels[0]).text = "%s:%s%s" % [time / 60, time % 60, s]
	get_node(right_labels[1]).text = str(max_combo)
	get_node(right_labels[2]).text = str(extra_balls)
	get_node(right_labels[3]).text = str(score)
	$Polygon2D2/Label2.visible = true
	end = true
	
	
func next_level():
	emit_signal("transition")
	queue_free()
	pass
	