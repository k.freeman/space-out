extends Node


func _ready():
	play_music("res://Assets/sfx/game_over_mast.wav")
	print(db2linear(0))
	pass # Replace with function body.

func _process(delta):
	if Input.is_action_just_pressed("ui_up"):
		mix_bus(2, 3)
	if Input.is_action_just_pressed("ui_down"):
		mix_bus(2, -3)

func play_music(track):
	$Music.stream = load(track)
	$Music.play(0)
	pass
	
func stop_music():
	pass

func play_sfx(sfx):
	pass
	
func stop_sfx():
	pass
		
func mute():
	pass
	
func mix_bus(bus_idx, db):
	AudioServer.set_bus_volume_db(bus_idx, db)
	
#	var vol = AudioServer.get_bus_volume_db(bus_idx)
#	if vol >= 0 && db > 0:
#		AudioServer.set_bus_volume_db(bus_idx, 0)
#		return
#	if vol <= -45 && db < 0:
#		AudioServer.set_bus_volume_db(bus_idx, -45)
#		return
#	AudioServer.set_bus_volume_db(bus_idx, vol + db)
#	if vol + db <= -45:
#		AudioServer.set_bus_mute(bus_idx, true)
#	elif vol + db > -45:
#		print("should unmute")
#		AudioServer.set_bus_mute(bus_idx, false)
		
func mix_sfx(db):
	pass
	
func mix_gui(db):
	pass


func _on_VSlider_value_changed(value):
	var db = linear2db(value)
	print(value)
	print(db)
	print(linear2db(0))
	mix_bus(2, db)
	pass # Replace with function body.
