extends Area2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var time_start
var state
export var amp = 2.0
export var period = 1
var step = PI/2
var dstep = 2*PI / 240
var speed

# Called when the node enters the scene tree for the first time.
func _ready():
	time_start = OS.get_ticks_msec()
	var tween = get_node("Tween")
	tween.interpolate_property($".", "position", Vector2(320, 0), Vector2(320,180), 2, Tween.TRANS_BACK, Tween.EASE_OUT)
	tween.start()
	
	pass # Replace with function body.
	
func _physics_process(delta):
	print(delta)
	var time = OS.get_ticks_msec() - time_start
	if state == "tweendone":
		sinewave()
	
func sinewave():
	step += dstep
#	transform.origin.x += amp * cos(step)
	position.x += amp * sin(step)

func _on_Tween_tween_completed(object, key):
	print("finished")
	print(object)
	print(key)
	state = "tweendone"
	pass # Replace with function body.

#func back_and_forth():
#	while !restart && !end:
#		tween.interpolate_property($".", "position", Vector2(
