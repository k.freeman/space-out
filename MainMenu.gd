extends Node

var camera
var labels = []
var shake_cam = false
var cur_selection = 0
var freeze = false

export var sounds = []

# Called when the node enters the scene tree for the first time.
func _ready():
	camera = $Camera2D
	camera.current = true
	labels.append($MarginContainer/HBoxContainer/StartGame)
	labels.append($MarginContainer/HBoxContainer/Options)
	labels.append($MarginContainer/HBoxContainer/Quit)
	$Camera2D/AnimationPlayer.play("Camera_position")
	update_selection(false)
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if shake_cam:
		camera.set_offset(Vector2( \
			rand_range(-1,1) * 5, \
			rand_range(-1,1) * 5 \
		))
	if Input.is_action_just_pressed("ui_left") && !freeze:
		if cur_selection > 0:
			cur_selection -= 1
		update_selection(true)
	if Input.is_action_just_pressed("ui_right") && !freeze:
		if cur_selection < 2:
			cur_selection += 1
		update_selection(true)
	if Input.is_action_just_pressed("ui_accept") && !freeze:
		choose_selection()
	
func shake_cam(time):
	$Camera2D/Shaketimer.wait_time = time
	shake_cam = true
	$Camera2D/Shaketimer.start()

func _on_Shaketimer_timeout():
	shake_cam = false

func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Camera_position":
		shake_cam(.6)

func update_selection(play):
	if play:
		$SelectSounds.stream = sounds[0]
		$SelectSounds.play(0)
	for i in range(labels.size()):
		if cur_selection != i:
			labels[i].add_color_override("font_color", Color(1,1,1,1))
		else:
			labels[i].add_color_override("font_color", Color("FFD800"))
			
func choose_selection():
	$SelectSounds.stream = sounds[1]
	match cur_selection:
		0:
			$MenuMusic.stop()
			$SelectSounds.stream = sounds[2]
			$SelectSounds.volume_db = -3
			$SelectSounds.play(0)
			freeze = true
			yield($SelectSounds, "finished")
			init_game()
		1:
			$SelectSounds.stream = sounds[1]
			$SelectSounds.play(0)
			print("selected options")
		2:
			$SelectSounds.stream = sounds[1]
			$SelectSounds.play(0)
			freeze = true
			yield(get_tree().create_timer(.2),"timeout")
			get_tree().quit()
			
func init_game():
	var game_instance = preload("res://GameInstance.tscn").instance()
	get_parent().add_child(game_instance)
	queue_free()
	pass