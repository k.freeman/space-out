extends Node2D

var velocity = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	velocity = Vector2(rand_range(-1,1), rand_range(-1,1))

func change_text(text):
	$Label.text = str(text)
	
func set_anim(anim):
	$AnimationPlayer.play(anim)

func _on_Timer_timeout():
	queue_free()
	
func _process(delta):
	position += velocity * delta
	
